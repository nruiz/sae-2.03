# Playbook deploy_template

```bash
---
- name: Déployer un fichier template 
  hosts: servers
  become: true
  vars:
    contenu_template: |
      Ceci est le contenu du fichier template.
      Il peut contenir du texte, des variables, etc.
  tasks:

    - name: Déployer le fichier template
      template:
        src: /home/testAnsible/venv/templates/template.j2
        dest: /home/testAnsible/venv/playbooks/deploye_template.yml
```

## Explication ligne par ligne : 

Les premières lignes sont identiques au premier Playbook.

### Ligne 5-8 : 
```bash 
vars:
    contenu_template: |
      Ceci est le contenu du fichier template.
      Il peut contenir du texte, des variables, etc.
```
Définit une variable contenu_template qui contient le contenu du fichier template. Le contenu est défini comme une chaîne de caractères multilignes.


### Ligne 9-10 : 
```bash 
tasks:
     - name: Déployer le fichier template
```
Définit une tâche nommée "Déployer le fichier template".
<br>

### Ligne 11-13 :
```bash 
template:
        src: /home/testAnsible/venv/templates/template.j2
        dest: /home/testAnsible/venv/playbooks/deploye_template.yml
```
Utilise le module template pour déployer un fichier template. <br> 
L'option src spécifie le chemin vers le fichier template source, et l'option dest spécifie le chemin de destination où le fichier généré sera déployé sur le serveur cible.
<br>


## Explication global de l'utilité du script :

Ce script permet de déployer un fichier template sur des serveurs spécifiés, en utilisant le contenu défini dans le playbook.



