# Playbook add_grp 

```bash
---
- name: Ajouter un groupe sur le serveur
  hosts: servers
  become: true
  vars:
    nom_grp : "{{ nom_goupe }}"
 
  tasks:
    - name: Créer un groupe
      group:
        name: "{{ nom_grp }}"
        state: present
```

## Explication ligne par ligne : 

### Ligne 1 : 
```bash 
---
```
Cette ligne indique le début d'un document YAML.
<br>

### Ligne 2 : 
```bash 
- name: Ajouter un groupe sur le serveur
```
Définit une tâche avec le nom "Ajouter un groupe sur le serveur".
<br>

### Ligne 3 : 
```bash 
hosts: servers
```
Spécifie les hôtes sur lesquels cette tâche sera exécutée. Dans cet exemple, la tâche sera exécutée sur les hôtes définis dans le groupe "servers".
<br>

### Ligne 4 : 
```bash 
become: true
```
Indique que la tâche doit être exécutée en tant qu'utilisateur disposant des privilèges de superutilisateur.
<br>

### Ligne 5-6 : 
```bash 
 vars:
    nom_groupe: "{{ nom_grp }}"
```
Définit une variable nommée nom_groupe avec la valeur de la variable nom_grp.
<br>

### Ligne 8 : 
```bash 
tasks:
```
Définit le début des tâches de cette tâche.
<br>

### Ligne 9 : 
```bash 
- name: Créer un groupe
```
Définit une tâche nommée "Créer un groupe".
<br>

### Ligne 10-11-12 : 
```bash 
group:
    name: "{{ nom_groupe }}"
    state: present
```
Utilise le module group pour créer un groupe avec le nom spécifié dans la variable nom_groupe. La valeur state: present indique que le groupe doit être présent (c'est-à-dire créé s'il n'existe pas).

<br>

## Explication global de l'utilité du script :

Ce script permet donc à l’utilisateur de saisir un nom de groupe en appellant la commande et cela va ensuite automatiquement créer un groupe de ce nom sur le serveur. 


